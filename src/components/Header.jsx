import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <div className='bg-indigo-800 text-white py-3 text-2xl'>
                Dashboard
            </div>
        )
    }
}

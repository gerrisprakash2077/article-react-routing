import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
export default class Home extends Component {
    render() {
        return (
            <div className='bg-teal-100 h-screen flex flex-col items-center'>
                <h2 className='py-11'>
                    🚀 Welcome to Homepage!
                </h2>
                <NavLink to='/Articles'>
                    <div className='bg-white my-200 mx-300  px-10 py-5 rounded-xl shadow-xl'>
                        <p className='underline'>ARTICLES PAGE</p>
                    </div>
                </NavLink>
            </div>
        )
    }
}

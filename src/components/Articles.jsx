import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import data from '../data/data.json'
import Article from './Article'
export default class Articles extends Component {
    constructor(props) {
        super(props)
        this.state = {
            articles: data,
            search: ''
        }
    }
    changeSearch = (event) => {
        let filteredArticle = data.filter((article) => {
            return article.title.toLowerCase().includes(event.target.value.toLowerCase())
        })
        this.setState({
            search: event.target.value,
            articles: filteredArticle
        })
    }
    render() {


        return (
            <div className='bg-teal-100 h-screen'>
                <input type="text" placeholder='Search' value={this.state.search} onChange={this.changeSearch} />
                {this.state.articles.map((element) => {
                    return (
                        <NavLink key={element.slug} to={`/Articles/${element.slug}`}>
                            <div className='p-10 pl:550px'>
                                <p className='text-2xl'>{element.title}</p>
                                <p>{element.author}</p>
                            </div>
                        </NavLink>
                    )
                })
                }
            </div >
        )
    }
}

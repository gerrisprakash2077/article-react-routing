// import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
// export default class Article extends Component {
//     render() {
//         // console.log(this.props.params);
//         return (
//             <div>Article
//                 <Link to='/Articles'><p>back</p></Link>
//             </div>
//         )
//     }
// }

import React from 'react'
import { Link, useParams } from 'react-router-dom'
export default function Article() {
    const params = useParams();
    return (
        <div className='bg-teal-100 h-screen'>
            <Link to='/Articles'><p className='underline'>Go back to articles</p></Link>
            <h2>The slug of the article is::: {params.id}</h2>
        </div>
    )
}


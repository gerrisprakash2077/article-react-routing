import React, { Component } from 'react'
import Header from './components/Header'
import Navbar from './components/Navbar'
import Articles from './components/Articles'
import Home from './components/Home'
import Article from './components/Article'
import { Route, Routes } from 'react-router-dom'
export default class App extends Component {
  render() {
    return (
      <>
        <Header />
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />}></Route>
          <Route path='/Articles' element={<Articles />}></Route>
          <Route path='/Articles/:id' element={<Article />}></Route>
          <Route path='/*' element={<p>NOT FOUND</p>} />
          <Route path='/Articles/*' element={<p>NOT FOUND</p>} />
        </Routes>
        {/* <Routes>
          <Route path='/' >
            <Home />
          </Route>
          <Route path='/Articles' >
            <Articles />
          </Route>
          <Route path='/Articles/:id' >
            <Article />
          </Route>
          <Route path='/*' >
            <p>NOT FOUND</p>
          </Route>
          <Route path='/Articles/*' >
            <p>NOT FOUND</p>
          </Route>
        </Routes> */}
      </>
    )
  }
}
